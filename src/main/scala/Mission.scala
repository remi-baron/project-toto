import scala.util.Random

var mission_to_print: Text_print = new Text_print
var mission_pending: Boolean = false
var current_mission: Mission = new Mission

class Mission(m_departure : City = null_city, m_arrival : City = null_city, m_reward : Int = 0):
    var mission_departure: City = m_departure
    var mission_arrival: City = m_arrival
    var mission_reward: Int = m_reward


def generate_mission() : Mission = {
    val i: Int = Random.nextInt(8)
    val departure_city = index_to_city(i)
    var j: Int = Random.nextInt(8)
    while(i == j) {
        j = Random.nextInt(8)
    }
    val arrival_city = index_to_city(j)
    val reward: Int = (departure_city.population / 2) * 10 
    val mission_s: String = "New mission : travel" + " \n"+ "from \n" + departure_city.name + " to \n" + arrival_city.name + "\nfor a reward of \n" + reward.toString +"$"
    mission_to_print = new Text_print(mission_s,20)
    new Mission(departure_city, arrival_city, reward)
}

def index_to_city(index : Int) : City = index match
    case 0 => station
    case 1 => city1
    case 2 => city2
    case 3 => city3
    case 4 => city4
    case 5 => city5
    case 6 => city6
    case 7 => city7
    case 8 => city8

