

import sfml.graphics.*
import sfml.window.*

//Class of the train line
class Train_line(city1: City, city2: City):
    var departure: City = city1
    var arrival: City = city2
    var train_type: String = "" 
    var train_on_rails: Int = 0 
    var occupation_time: Int = 0 
    var level: Int = 1 /* 1 = black, 2 = red, 3 = blue, 4 = white */
    var color: Color = Color.Black()
    def to_text() = {
        f"${departure.name} ${arrival.name} ${level}"
    }