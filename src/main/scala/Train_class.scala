
import sfml.graphics.*
import sfml.window.*

//Class of the train
class Train(train_number: Int = 0, departure: City = null_city, arrival: City = null_city, dir_x: Float = 0f, dir_y: Float = 0f, s: Float = 1f, time: Int =0, x: Float = 0f, y: Float= 0f, txt_name : String = "train"):
    var id: Int = train_number
    var texture_name: String = txt_name
    var position_x: Float = x
    var position_y: Float = y
    var direction_x: Float = dir_x
    var direction_y: Float = dir_y
    var capacity: Int = 100
    var speed: Float = s
    var train_type: String = ""
    var train_departure: City = departure
    var train_arrival: City  = arrival
    var travel_time: Int = time

    def update_pos() = {
    travel_time = travel_time - 1
    position_x = position_x + direction_x*speed
    position_y = position_y + direction_y*speed      
}

//class Boat() extends Train(): ce serait mieux, mais on rajouterai quoi comme champ enfait ????
class Boat (train_number: Int = 0, departure: City = null_city, arrival: City = null_city, dir_x: Float = 0f, dir_y: Float = 0f, s: Float = 1f, time: Int =0, x: Float = 0f, y: Float= 0f) extends Train(train_number, departure, arrival, dir_x, dir_y, s, time, x, y):
    train_type = "boat"
    texture_name = "boat"
    capacity = 90

class Plane (train_number: Int = 0, departure: City = null_city, arrival: City = null_city, dir_x: Float = 0f, dir_y: Float = 0f, s: Float = 1f, time: Int =0, x: Float = 0f, y: Float= 0f) extends Train(train_number, departure, arrival, dir_x, dir_y, s, time, x, y):
    train_type = "plane"
    texture_name = "plane"
    capacity = 10

class Resource_train(train_number: Int = 0, departure: City = null_city, arrival: City = null_city, dir_x: Float = 0f, dir_y: Float = 0f, s: Float = 1f, time: Int =0, x: Float = 0f, y: Float= 0f, res_on : Resource) extends Train(train_number, departure, arrival, dir_x, dir_y, s, time, x, y):
    var resources_on : Resource = res_on
    train_type = "train"
    texture_name = "train"
    capacity = 250

class Resource_boat (train_number: Int = 0, departure: City = null_city, arrival: City = null_city, dir_x: Float = 0f, dir_y: Float = 0f, s: Float = 1f, time: Int =0, x: Float = 0f, y: Float= 0f, res_on : Resource) extends Train(train_number, departure, arrival, dir_x, dir_y, s, time, x, y):
    var resources_on : Resource = res_on
    train_type = "boat"
    texture_name = "boat"
    capacity = 1000



