//Work in progress, do not disturb


import java.awt.image.BufferedImage
import java.io._
import sfml.graphics.*
import sfml.window.*

object PerlinNoise {
  private def fade(t: Double): Double = t * t * t * (t * (t * 6 - 15) + 10)

  private def lerp(t: Double, a: Double, b: Double): Double = a + t * (b - a)

  private def grad(hash: Int, x: Double, y: Double): Double = {
    val h = hash & 15
    val u = if (h < 8) x else y
    val v = if (h < 4) y else if (h == 12 || h == 14) x else 0
    if ((h & 1) == 0) u else -u + (if ((h & 2) == 0) v else -v)
  }

  private val permutation: Array[Int] = (0 to 255).toArray ++ (0 to 255).toArray
  private val p: Array[Int] = {
    val perm = scala.util.Random.shuffle(permutation.toList).toArray
    perm ++ perm
  }

  def noise(x: Double, y: Double): Double = {
    val xi = x.toInt & 255
    val yi = y.toInt & 255
    val xf = x - x.toInt
    val yf = y - y.toInt
    val u = fade(xf)
    val v = fade(yf)
    val aa = p(p(xi) + yi)
    val ab = p(p(xi) + yi + 1)
    val ba = p(p(xi + 1) + yi)
    val bb = p(p(xi + 1) + yi + 1)

    val x1 = lerp(u, grad(aa, xf, yf), grad(ba, xf - 1, yf))
    val x2 = lerp(u, grad(ab, xf, yf - 1), grad(bb, xf - 1, yf - 1))
    lerp(v, x1, x2)
  }
}

// Using PerlinNoise object
def create_perlin_noise (): List[List[Double]] = {
  val width = 100
  val height = 100
  val scale = 0.1
  var my_list = List[List[Double]]()

  for (y <- 0 until height) {
    var row = List[Double]()
    for (x <- 0 until width) {
      val value = PerlinNoise.noise(x * scale, y * scale)
      row = row :+ ((value + 1) * 0.5)%1.2f
    }
    my_list = my_list :+ row
  }
  return my_list
}

def print_map (perlin_noise : List[List[Double]]) = {
  for (y <- 0 until 100) {
    for (x <- 0 until 100) {
      if (perlin_noise(y)(x) < 0.3) {
        print(" ")
      } else if (perlin_noise(y)(x) < 0.4) {
        print(".")
      } else if (perlin_noise(y)(x) < 0.6) {
        print("o")
      } else if (perlin_noise(y)(x) < 0.8) {
        print("O")
      } else {
        print("0")
      }
    }
    println()
  }
}

def double_array_to_color_array(array : List[List[Double]])={
  var pixels_array = List[List[Int]]()
  for (y <- 0 until array.length){
    var row = List[Int]()
    for (x <- 0 until array(0).length){
      if (array(y)(x) < 0.5) {
        row = 255 :: row
      }
      if (array(y)(x) < 0.6) {
        row = 256*256*255 + 256*255 :: row
      }
      if (array(y)(x) < 0.7) {
        row = 256*255 :: row
      }
      if (array(y)(x) < 0.8) {
        row = 256*256*255 :: row
      } 
      else {
        row = 255*(256*256) + 255*256 + 255 :: row
      }
    }
    pixels_array = row :: pixels_array
  }
  pixels_array
}


def array_to_image(array: List[List[Int]]) = {
  val width = array(0).length
  val height = array.length
  val image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
  for (y <- 0 until height) {
    for (x <- 0 until width) {
      image.setRGB(x, y, array(y)(x))
    }
  }
  image
}

/*
def save_image(image : BufferedImage, filename : String) = {
  ImageIO.write(image, "png", new File(RESOURCES_DIR + filename + ".png"))
}

def write_array(array : List[List[Int]], filename : String) = {
  val file = new PrintWriter(new File(RESOURCES_DIR + filename + ".txt"))
  for (y <- 0 until array.length) {
    for (x <- 0 until array(0).length) {
      file.write(array(y)(x).toString + " ")
    }
    file.write("\n")
  }
}

def generate_map() = {
  val perlin_noise = create_perlin_noise()
  print_map(perlin_noise)
  val color_array = double_array_to_color_array(perlin_noise)
  write_array(color_array, "map")
  val toto = new ImageGenerator
  val titi = toto.arrayToImage ()
  toto.saveImage(titi, "map", "resources")
}

*/