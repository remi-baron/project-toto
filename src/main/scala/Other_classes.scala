import sfml.graphics.*
import sfml.window.*

val null_city = new City

//Class of text to be displayed on the bottom of the screen
class Text_print(t: String = "", s: Int = 20):
    var text: String = t
    var size: Int = s
    var time_to_live: Float = (32*1).toFloat