import scala.io.Source
import java.io._
import sfml.graphics.*

def load_save () = {
    var read_mode = 0
    //Check if file exists
    if (File(RESOURCES_DIR+"save.txt").exists()) {
        val bufferedSource = Source.fromFile(RESOURCES_DIR+"save.txt")
        train_lines_list = List()
        for (line <- bufferedSource.getLines) {
            line match {
                case "CITIES" => read_mode = 1
                case "TRAIN LINES" => read_mode = 2
                case "TRAIN NUMBER" => read_mode = 3
                case "MONEY" => read_mode = 4
                case _ => {
                    val words = line.split(" ")
                    if (read_mode == 1) {
                        for (city <- city_list) {
                            if (city.name == words(0)) {
                                city.population = words(2).toInt
                                city.number_of_train = words(3).toInt
                            }
                            for (i <- 7 until words.length) {
                                city.city_links_name = words(i) :: city.city_links_name
                            }
                        }
                    }
                    if (read_mode == 2) {
                        val train_line = new Train_line(null_city, null_city)
                        train_line.departure = city_list.filter(_.name == words(0))(0)
                        train_line.arrival = city_list.filter(_.name == words(1))(0)
                        train_line.level = words(2).toInt
                        train_line.color = train_line.level match {
                            case 1 => Color.Black()
                            case 2 => Color.Red()
                            case 3 => Color.Blue()
                            case 4 => Color.White()
                        }
                        train_lines_list = train_line :: train_lines_list
                    }
                    if (read_mode == 3) {
                        train_number = words(0).toInt
                    }
                    if (read_mode == 4) {
                        money = words(0).toInt
                    }
                }
            }
            
        }
        bufferedSource.close
        for (city <- city_list) {
            city.city_links_name_to_city()
        }
    }
}

def write_save () = {
    val pw = new PrintWriter(new File(RESOURCES_DIR+"save.txt" ))
    pw.write("CITIES\n")
    for (city <- city_list) {
        pw.write(city.to_text () + "\n") 
    }
    pw.write("TRAIN LINES\n")
    for (train_line <- train_lines_list) {
        pw.write(train_line.to_text () + "\n")
    }
    pw.write("TRAIN NUMBER\n")
    pw.write(train_number.toString + "\n")
    pw.write("MONEY\n")
    pw.write(money.toString)
    pw.close
}