import scala.util.Using

import sfml.graphics.*
import sfml.window.*


def get_sprite(name: String) = {
            if (sprites_map.contains(name)) then{
                sprites_map(name)
            }
            else{
                val texture = Texture()
                    if !(texture.loadFromFile(RESOURCES_DIR + name + ".png")) then System.exit(1)
                val sprite = Sprite(texture)
                sprite
            }
        }

def move_map(view: View, window: RenderWindow, key: Keyboard.Key) = {
    val map_move_speed: Float = 16f
    key match
        case Keyboard.Key.KeyUp    => if (view.center.y - 325 > 0) view.move(0, -map_move_speed)
        case Keyboard.Key.KeyDown  => if (view.center.y + 325 < 1600) view.move(0, map_move_speed)
        case Keyboard.Key.KeyLeft  => if (view.center.x - 400 > 0) view.move(-map_move_speed, 0)
        case Keyboard.Key.KeyRight => if (view.center.x + 400 < 1600) view.move(map_move_speed, 0)
        case _         => ()
    window.view = view
    
}

def show_map(view: View, window: RenderWindow) = {
    val map_sprite = get_sprite("map")
    map_sprite.position = (0, 0)
    window.draw(map_sprite)
}

def show_map1(view: View, window: RenderWindow) = {
    var pos_x:Int = 0
    var pos_y:Int = 0
    for (y <- 0 until 100) {
        for (x <- 0 until 100) {
        val rect = RectangleShape((16, 16))
        rect.position = (pos_x , pos_y)
        if (map(y)(x) < 0.3) {
            rect.fillColor = Color.Blue()
        } else if (map(y)(x) < 0.45) {
            rect.fillColor = Color.Yellow()
        } else if (map(y)(x) < 0.65) {
            rect.fillColor = Color.Green()
        } else if (map(y)(x) < 0.75) {
            rect.fillColor = Color(100,100,100)
        } else {
            rect.fillColor = Color.White()
        }
        window.draw(rect)
        rect.close()
        pos_x += 16
        }
        pos_x = 0
        pos_y += 16
    }
    
}



def draw_cities(window: RenderWindow) = {
    for (city <- city_list) {
        if (city.highlighted) {
            val rectangle = RectangleShape((72, 72))
            rectangle.fillColor = Color(255, 255, 255)
            rectangle.position = (city.position_x - 4, city.position_y - 4)
            window.draw(rectangle)
        }
        val city_sprite = get_sprite(city.texture_name)
        city_sprite.position = (city.position_x, city.position_y)
        window.draw(city_sprite)
        if (city.buttons_shown) {
            val buttons_sprite = get_sprite("buttons")
            buttons_sprite.position = (city.position_x, city.position_y)
            window.draw(buttons_sprite)
        }
    }
}

def draw_train_lines(window: RenderWindow) = {
    for (train_line <- train_lines_list) {
        window.draw(draw_train_line(train_line))
    }
}

def draw_trains(window: RenderWindow) = {
    for (train <- trains_list) {
        val train_sprite = get_sprite(train.texture_name)
        train_sprite.position = (train.position_x, train.position_y)
        window.draw(train_sprite)
    }
}

def draw_money(window: RenderWindow, font: Font, view1:View) = {
    Using.Manager { use =>
        val text = use(Text(f"Money : ${money}", font, 20))
        text.position = (view1.center.x + 200, view1.center.y - 325)
        text.fillColor = Color.White()
        window.draw(text)
    }.get
}