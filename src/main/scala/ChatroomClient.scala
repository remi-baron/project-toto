import java.net.Socket
import java.io.{BufferedReader, InputStreamReader, PrintWriter, File}
import java.util.concurrent.{Executors, TimeUnit}
import scala.io.StdIn
import scala.concurrent.ExecutionContext
import scala.concurrent.ExecutionContext.Implicits.global

object ChatClient extends App {
  // Replace with the actual server IP address
  val serverIpAddress = "172.23.149.120" // Replace with your server's IP address
  val client = new Socket(serverIpAddress, 9999)
  val in = new BufferedReader(new InputStreamReader(client.getInputStream))
  val out = new PrintWriter(client.getOutputStream, true)

  // Ensure save.txt exists for the host and query.txt exists for all clients
  val saveFile = new File("src/main/resources/save.txt")
  val queryFile = new File("src/main/resources/query.txt")

  // Prompt for a username
  val username = StdIn.readLine("Enter your username: ")
  out.println(username)

  println(s"Connected to game server as $username")

  // Thread to listen for messages from the server
  val listener = new Thread(new Runnable {
    def run(): Unit = {
      var running = true
      while (running) {
        val response = in.readLine()
        if (response != null) {
          if (response.startsWith("SAVE_DATA:")) {
            // Handle save data update from server
            val saveData = response.stripPrefix("SAVE_DATA:")
            writeToFile("src/main/resources/save.txt", saveData)
            println("Updated save.txt with new data from server.")
          } else if (response.startsWith("QUERY_DATA:")) {
            // Handle query data update from server
            val queryData = response.stripPrefix("QUERY_DATA:")
            writeToFile("src/main/resources/query.txt", queryData)
            println("Updated query.txt with new data from server.")
          } else {
            println(response)
          }
        } else {
          running = false
        }
      }
    }
  })

  listener.start()

  // Scheduled task for the host to periodically send save data to the server
  if (username == "host") {
    val scheduler = Executors.newScheduledThreadPool(1)
    val periodicTask = new Runnable {
      def run(): Unit = {
        val saveData = readFromFile("src/main/resources/save.txt")
        out.println(s"SAVE_DATA:$saveData")
      }
    }
    scheduler.scheduleAtFixedRate(periodicTask, 0, 5, TimeUnit.SECONDS)
  } else {
    // Scheduled task for non-host clients to periodically send query data to the server
    val scheduler = Executors.newScheduledThreadPool(1)
    val periodicTask = new Runnable {
      def run(): Unit = {
        val queryData = readFromFile("src/main/resources/query.txt")
        out.println(s"QUERY_DATA:$queryData")
      }
    }
    scheduler.scheduleAtFixedRate(periodicTask, 0, 5, TimeUnit.SECONDS)
  }

  // Main loop to read user input and send chat messages
  var running = true
  while (running) {
    val message = StdIn.readLine()
    if (message != null && message != "exit") {
      out.println(message)
    } else {
      running = false
    }
  }

  client.close()
  println("Disconnected from game server")

  def readFromFile(filename: String): String = {
    scala.io.Source.fromFile(filename).getLines.mkString("\n")
  }

  def writeToFile(filename: String, data: String): Unit = {
    val pw = new PrintWriter(new File(filename))
    pw.write(data)
    pw.close()
  }
}
