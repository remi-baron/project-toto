import java.net.{ServerSocket, Socket}
import java.io.{BufferedReader, InputStreamReader, PrintWriter}
import scala.concurrent.ExecutionContext
import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global

object ChatServer extends App {
  val server = new ServerSocket(9999)
  println("Game server started on port 9999")

  val clients = mutable.Map[PrintWriter, String]() // Map to store clients and their usernames
  var host: Option[PrintWriter] = None // Track the "host" client

  while (true) {
    val client = server.accept()
    handleClient(client)
  }

  def handleClient(client: Socket): Unit = {
    val in = new BufferedReader(new InputStreamReader(client.getInputStream))
    val out = new PrintWriter(client.getOutputStream, true)

    // Read the username
    val username = in.readLine()
    println(s"User connected: $username")

    clients.synchronized {
      clients += (out -> username)
      if (username == "host") {
        host = Some(out)
      }
    }

    broadcastMessage(s"$username has joined the game.", out)

    var running = true
    while (running) {
      val message = in.readLine()
      if (message != null) {
        println(s"Received from $username: $message")

        if (username == "host") {
          if (message.startsWith("SAVE_DATA:")) {
            val saveData = message.stripPrefix("SAVE_DATA:")
            println(s"Received save data from host")

            // Broadcast save data to all clients
            broadcastSaveData(saveData)
          } else {
            // Regular chat message from host
            broadcastMessage(s"$username: $message", out)
          }
        } else {
          if (message.startsWith("QUERY_DATA:")) {
            val queryData = message.stripPrefix("QUERY_DATA:")
            println(s"Received query data from $username")

            // Broadcast query data to all clients
            broadcastQueryData(queryData)
          } else {
            // Regular chat message from other clients
            broadcastMessage(s"$username: $message", out)
          }
        }
      } else {
        running = false
      }
    }

    clients.synchronized {
      clients -= out
      if (username == "host") {
        host = None
      }
    }
    broadcastMessage(s"$username has left the game.", out)
    client.close()
    println(s"User disconnected: $username")
  }

  def broadcastMessage(message: String, sender: PrintWriter): Unit = {
    clients.synchronized {
      clients.foreach { case (client, _) =>
        if (client != sender) {
          client.println(message)
        }
      }
    }
  }

  def broadcastSaveData(saveData: String): Unit = {
    clients.synchronized {
      clients.foreach { case (client, _) =>
        client.println(s"SAVE_DATA:$saveData")
      }
    }
  }

  def broadcastQueryData(queryData: String): Unit = {
    clients.synchronized {
      clients.foreach { case (client, _) =>
        client.println(s"QUERY_DATA:$queryData")
      }
    }
  }
}
