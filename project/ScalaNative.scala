import sbt.{AutoPlugin, fileToRichFile}
import sbt.Keys.{baseDirectory, envVars}

import BuildMode.autoImport.*

import scala.scalanative.build.{Mode, LTO}
import scala.scalanative.sbtplugin.ScalaNativePlugin
import scala.scalanative.sbtplugin.ScalaNativePlugin.autoImport.*

object ScalaNative extends AutoPlugin {
    override def requires = BuildMode && ScalaNativePlugin

    override lazy val projectSettings = Seq(
        envVars := {
            val leakFile = baseDirectory.value / "project" / "leak.txt"

            buildMode.value match {
                case BuildMode.Debug | BuildMode.CI =>
                    Map("LSAN_OPTIONS" -> f"print_suppressions=false:suppressions=${leakFile}")
                case _ => Map.empty
            }
        },
        nativeConfig := {
            buildMode.value match {
                case BuildMode.Debug | BuildMode.CI =>
                    nativeConfig.value
                        .withIncrementalCompilation(true)
                        .withLTO(LTO.none)
                        .withMode(Mode.debug)
                        .withCompileOptions(Seq("-fsanitize=leak"))
                        .withLinkingOptions(Seq("-fsanitize=leak", "-fuse-ld=lld"))

                case BuildMode.Release =>
                    nativeConfig.value
                        .withLTO(LTO.thin)
                        .withMode(Mode.release)
                        .withLinkingOptions(Seq("-fuse-ld=lld"))
            }
        }
    )
}
