
/*
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.FileNotFoundException;




public class ImageGenerator {

    public BufferedImage arrayToImage() {
        File map = new File("resources/map.txt");
        List<List<Integer>> array = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(map);
            while (scanner.hasNextLine()) {
                List<Integer> row = new ArrayList<>();
                String line = scanner.nextLine();
                for (int i = 0; i < line.length(); i++) {
                    // Convert a string where int are separated by a space to a list of int
                    row.add(Integer.parseInt(line.split(" ")[i]));
                }
                array.add(row);
            }
        } catch (FileNotFoundException e) {
            System.out.println("Error reading file: " + e.getMessage());
        }
        int width = array.get(0).size();
        int height = array.size();
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                image.setRGB(x, y, array.get(y).get(x));
            }
        }
        return image;
    }

    public void saveImage(BufferedImage image, String filename, String resourcesDir) {
        String filePath = "resources" + filename + ".png";
        try {
            File output = new File(filePath);
            ImageIO.write(image, "png", output);
            System.out.println("Image saved to: " + filePath);
        } catch (IOException e) {
            System.out.println("Error saving image: " + e.getMessage());
        }
    }


}

*/