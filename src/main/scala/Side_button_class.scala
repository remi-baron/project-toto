

import sfml.graphics.*
import sfml.window.*

//Class of the side buttons, used to change the game mode. They are the buttons on the big green thing in the right of the window.
class Side_button(x1: Float, y1: Float, x2: Float, y2: Float, name: String, city_attached: City):
    var px1: Float = x1
    var py1: Float = y1
    var px2: Float = x2
    var py2: Float = y2
    var button_name : String = name
    var city: City = city_attached