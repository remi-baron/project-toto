import sfml.graphics.*
import sfml.window.*

//City class
class City(city_name : String = "", text_name : String = "", x : Float = 0, y : Float = 0, city_biome : Biome = forest):
    var name: String = city_name
    var texture_name: String = text_name
    var population: Int = 100
    var number_of_train: Int = 0
    var city_links : List[City] = List()
    var authorized_roads : List[String] = List()
    var city_links_name : List[String] = List()
    //var production: List[Resource_type] = r_production // nom des différentes ressources que peut produire la ville
    var biome : Biome = city_biome
    var resources: List[Resource] = new Resource() :: Nil
    var position_x: Float = x
    var position_y: Float = y
    var size_x: Float = 64
    var size_y: Float = 64
    var highlighted: Boolean = false
    var buttons_shown: Boolean = false
    var number_of_planes: Int = 0
    var airport_level: Int = 0
    var harbour_links : List[String] = List()


    def city_links_name_to_city() = {
        for (city_name <- city_links_name) {
            city_links = city_list.filter(_.name == city_name)(0) :: city_links
        }
    }

    def text_city_links() = {
        var s: String = ""
        for (city <- city_links) {
            s = s + city.name + " "
        }
        s
    }


    //Function to display the city's data
    def to_text() = {
        val text: String = text_city_links()
        f"${name} ${texture_name} ${population} ${number_of_train} ${position_x} ${position_y} ${biome.biome_name}" + text
    }

    //Called when player clicks on the city. Entirely depends on the current game_mode.
    def is_clicked(is_shift: Boolean) = {

        //If the game mode is 0, the player is in the default mode. He can see the city's stats.
        if (game_mode == 0) then {
            println(f"City ${name} is clicked")
            val s = f"There is ${population}"+ " people in " + name + ", and " + number_of_train.toString() + " trains in town"
            text_to_print = new Text_print(s, 20)
            for (city <- city_list) {
                city.highlighted = false
                city.buttons_shown = false
            }
            buttons_shown = true
            val create_train_button: Side_button = new Side_button(x1 = position_x + 2, y1 = position_y + 2, x2 = position_x + 31, y2 = position_y + 31, name = "create train", city_attached = this)
            val create_train_line_button: Side_button = new Side_button(x1 = position_x + 33, y1 = position_y + 2, x2 = position_x + 62, y2 = position_y + 31, name = "create train line", city_attached = this)
            val show_line_level_button: Side_button = new Side_button(x1 = position_x + 2, y1 = position_y + 33, x2 = position_x + 31, y2 = position_y + 62, name = "line level", city_attached = this)
            val create_trip_button: Side_button = new Side_button(x1 = position_x + 33, y1 = position_y + 33, x2 = position_x + 62, y2 = position_y + 62, name = "create trip", city_attached = this)
            side_buttons = create_trip_button :: show_line_level_button :: create_train_button :: create_train_line_button :: Nil
            this.highlighted = true
            first_city_clicked = this :: Nil
        }

        //If the game mode is 1, the player is creating a train line. He can click on two cities to create a train line between them.
        //If the train_line already exists, it is upgraded instead.
        else if ((game_mode == 1)&&(first_city_clicked.head != this)) then {

            //If a city has already been clicked on while in this game_mode, clicking on this city directly creates/upgrades the train_line.
            // this et create_train_line_button.city
            var should_create: Boolean = true
            //if (first_city_clicked.name != ""){ // test pour vérifier que first_city n'est pas null city, et que donc on a bien déjà cliqué sur une ville
                for (train_line <- train_lines_list){
                    if ((train_line.departure == first_city_clicked.head && (train_line.arrival == this)) || (train_line.departure == this && (train_line.arrival == first_city_clicked.head))) then {                             
                        val my_text: Text_print = new Text_print("Upgraded train line", 20)
                        should_create = false
                        text_to_print = my_text
                        if (train_line.level == 1) {
                            train_line.color = Color.Red()
                            train_line.level = 2
                        }
                        else if (train_line.level == 2) {
                            train_line.color = Color.Blue()
                            train_line.level = 3
                        }
                        else if (train_line.level == 3) {
                            train_line.color = Color.White()
                            train_line.level = 4
                        }
                    } 
                //}
            }
            if (should_create && first_city_clicked.head.authorized_roads.contains(this.name)) then {
                val my_text: Text_print = new Text_print("Created train line", 20)
                text_to_print = my_text
                city_links = first_city_clicked.head :: city_links
                city_links_name = first_city_clicked.head.name :: city_links_name
                first_city_clicked.head.city_links = this :: first_city_clicked.head.city_links
                first_city_clicked.head.city_links_name = this.name :: first_city_clicked.head.city_links_name
                val train_line: Train_line = new Train_line(first_city_clicked.head, this)
                train_lines_list = train_line :: train_lines_list
            }
            else {
                val my_text: Text_print = new Text_print("Train line impossible", 20)
                text_to_print = my_text
                //refund money
                money +=100
            }
            game_mode = 0
            money -= 100
            first_city_clicked.head.highlighted = false
        }

        //If the game mode is 2, the player is checking the train line's info. He can click on two cities to see the train line's length and level.        
        else if ((game_mode == 2)&&(first_city_clicked.head != this)) then {
            //if (first_city_clicked.name != "") {
            //If a city has already been clicked on while in this game_mode, clicking on this city directly shows the train line's length and level.
            for (train_line <- train_lines_list){
                if ((train_line.departure == this && train_line.arrival == first_city_clicked.head) || (train_line.departure == first_city_clicked.head && train_line.arrival == this)) then{
                    val distance: Int = math.sqrt(math.pow((this.position_x - first_city_clicked.head.position_x).toDouble, 2) + math.pow((this.position_y - first_city_clicked.head.position_y).toDouble, 2)).toInt
                    val train_line_string1: String = " and its " + distance.toString() + " pixels long"
                    val train_line_string: String = "The level of this train line is " + train_line.level.toString() + train_line_string1
                    val train_line_text: Text_print = new Text_print(train_line_string, 20)
                    text_to_print = train_line_text
                }
            }
            game_mode = 0
            first_city_clicked.head.highlighted = false
        //}
        }

        //If the game mode is 3, the player is creating a trip. He can click on two cities to create a trip between them.
        else if ((game_mode == 3)&&(first_city_clicked.head != this)) then {

            //If a city has already been clicked on while in this game_mode, clicking on this city directly creates a trip between the two cities.
            //if (first_city_clicked.name != "") {
                var is_there_train_line: Boolean = false
                var speed: Int = 0
                for (train_line <- train_lines_list) {
                    if ((train_line.departure == this && train_line.arrival == first_city_clicked.head) || (train_line.departure == first_city_clicked.head && train_line.arrival == this)) then{
                        is_there_train_line = true
                        speed = train_line.level
                    }
                }
                if (first_city_clicked.head.number_of_train > 0) then {
                    create_trip(first_city_clicked.head, this, speed.toFloat, is_there_train_line, is_shift)
                    // We now check if we realised the current mission or not 

                    if (current_mission.mission_departure == first_city_clicked.head && current_mission.mission_arrival == this) {
                        money = money + current_mission.mission_reward
                        mission_pending = false //nécessaire pour qu'une nouvelle mission soit générée
                    }
                }
                else {
                    val train_number_string: String = "There are currently no possible trips between these two cities."
                    val train_number_text: Text_print = new Text_print(train_number_string, 20)
                    text_to_print = train_number_text
                }
                game_mode = 0
                first_city_clicked.head.highlighted = false
            //}
        }
    }

//Instantiate station, the city where all trains are born
var station: City = new City(city_name = "The_station", text_name = "station", x = 30f, y = 30f)



 /*Create all cities*/
val city1 = new City(city_name = "North_harbour", text_name = "harbour_city", x = 15f, y = 380f, city_biome = sea)   

val city2 = new City(city_name = "The_mine", text_name = "coal_city", x = 850f, y = 20f, city_biome = mine)

val city3 = new City(city_name = "The_forest_town", text_name = "town", x = 560f, y = 100f, city_biome = forest)

val city4 = new City(city_name = "The_desert_temple", text_name = "sand_city", x = 100f, y = 800f, city_biome = desert)

val city5 = new City(city_name = "The_lonely_mountain", text_name = "mountain_city", x = 850f, y = 700f, city_biome = mountain)

val city6 = new City(city_name = "Atlantis", text_name = "fish_city", x = 1100f, y = 400f, city_biome = sea)

val city7 = new City(city_name = "South_harbour", text_name = "harbour_city", x = 200f, y = 600f, city_biome = sea)

val city8 = new City(city_name = "East_harbour", text_name = "harbour_city", x = 770f, y = 600f, city_biome = sea)



def init_harbour() = {
    city1.harbour_links = city6.name :: city7.name :: city8.name :: Nil
    city6.harbour_links = city1.name :: city7.name :: city8.name :: Nil
    city7.harbour_links = city1.name :: city6.name :: city8.name :: Nil
    city8.harbour_links = city1.name :: city6.name :: city7.name :: Nil
    
}


def authorized_roads()= {
    station.authorized_roads = city1.name :: city2.name :: city3.name :: Nil
    city1.authorized_roads = station.name :: city2.name :: city3.name :: Nil
    city2.authorized_roads = station.name :: city1.name :: city3.name :: Nil
    city3.authorized_roads = station.name :: city1.name :: city2.name :: Nil
    city4.authorized_roads = city7.name :: Nil
    city5.authorized_roads = city8.name :: Nil
    city6.authorized_roads = Nil
    city7.authorized_roads = city4.name :: Nil
    city8.authorized_roads = city5.name :: Nil
}