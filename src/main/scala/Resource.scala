class Resource_type(r_t_name : String = "", r_t_price : Int = 10):
    var name: String = r_t_name // sert aussi de sprite name ?
    var price: Int = r_t_price

class Resource(resource_type : Resource_type = stone, resource_amount : Int = 10):
    var res_type: Resource_type = resource_type
    var amount: Int = resource_amount

val coal : Resource_type = new Resource_type("coal", 100)

val wood : Resource_type = new Resource_type("wood", 50)

val stone : Resource_type = new Resource_type("stone", 50)

val fish : Resource_type = new Resource_type("fish", 10)

val glass : Resource_type = new Resource_type("glass", 150)

//------------------gestion des biomes---------------------------------------------

class Biome(name : String = "", production : List[Resource_type] = List(coal)):
    var biome_production = production
    var biome_name = name

val mine : Biome = new Biome("mine", coal :: Nil)

val sea : Biome = new Biome("sea", fish :: Nil)

val desert : Biome = new Biome("desert", glass :: Nil)

val mountain : Biome = new Biome("mountain", stone :: coal:: Nil)

val forest : Biome = new Biome("forest", wood :: Nil)

val empty_resource = new Resource
val null_biome = new Biome


/*trait Resource:
    var name: String
    var amount: Int
    var price: Int
    var production: Int

trait Food extends Resource:
    var name: String
    var amount: Int
    var price: Int
    var production: Int
    var consumption: Int*/