
import scala.math.sqrt
import scala.math.atan2
import sfml.graphics.*
import sfml.window.*

/*Game_mode : 0 if normal, 1 if creating or upgrading a train_line, 2 if inspecting a train_line, 3 if creating a trip.
4 if creating a train.
The game_mode is equal to -1 at the beginning, which corresponds to the home screen. It will never return to -1 afterwards.
Game_mode can be changed by navigating through the buttons on the right, on the menu (and with the Control key).*/

var game_mode: Int = -1

/*Only used in game_mode 1, 2 and 3. 
This list contains the zero then one then two cities selected for the : creation of a train_line, creation of a trip, etc (depending on game_mode)*/

var cities_for_train_line: List[City] = List()

//val create_train_button: Side_button = new Side_button(x1 = 630f, y1 = 40f, x2 = 775f, y2 = 135f, name = "create train")
//val create_train_line_button: Side_button = new Side_button(x1 = 630f, y1 = 155f, x2 = 775f, y2 = 255f, name = "create train line")
//val show_line_level: Side_button = new Side_button(x1 = 630f, y1 = 280f, x2 = 775f, y2 = 375f, name = "line level")
//val create_trip_button: Side_button = new Side_button(x1 = 630f, y1 = 390f, x2 = 775f, y2 = 480f, name = "create trip")

/*We define interesting global variables.
For example, we define all the lists that will contain the every object of the interesting classes of the game.*/

var train_number: Int = 0
var trains_list: List[Train] = List()
var city_list: List[City] = List()
var train_lines_list: List[Train_line] = List()
var resource_list: List[Resource] = List()
var text_to_print: Text_print = new Text_print("Welcome to SNCF Simulator", 20)
var side_buttons: List[Side_button] = List() //create_trip_button :: show_line_level :: create_train_button :: create_train_line_button :: Nil
var first_city_clicked: List[City] = List()
var money:Int = 300
var map: List[List[Double]] = List()

/*The who_is_clicked function is an explicit function, it is called every time the player clicks (actually, every time he releases the Alt key as Alt replaces the click in this game.)
It takes the coordinates of the mouse as arguments.
It iterates through all the cities and all the buttons to determine if the mouse is on an interesting object when the click is performed, in order to do the corresponding action.*/

def who_is_clicked(mouse_position_x: Float, mouse_position_y: Float, view: View, is_shift: Boolean) = {

    val top_left_coord_x = view.center.x - view.size.x / 2
    val top_left_coord_y = view.center.y - view.size.y / 2

    who_is_clicked_buttons(mouse_position_x, mouse_position_y)

    if ((mouse_position_x.toInt <= top_left_coord_x.toInt + 600) && (mouse_position_y.toInt <= top_left_coord_y.toInt + 600)){
        who_is_clicked_cities(mouse_position_x, mouse_position_y,is_shift)
    }
    
    //For debug purpose only
    println(f"Position x = ${mouse_position_x}")
    println(f"Position y = ${mouse_position_y}")
    println(f"game_mode = ${game_mode}")


    
    }

def who_is_clicked_cities(mouse_position_x: Float, mouse_position_y: Float, is_shift: Boolean) = {
    //Iterate through all the cities
    for (city <- city_list) {

        if (city.buttons_shown==false) {    

        //For debug purpose only
        println(f"City ${city.name} position x = ${city.position_x} position y = ${city.position_y}")

        //If city is clicked, city.is_clicked, defined in Classes.scala, is called.
        if ((mouse_position_x.toInt >= city.position_x.toInt) ) {
            if ((mouse_position_x.toInt <= city.position_x.toInt + city.size_x.toInt)) {
                if ((mouse_position_y.toInt >= city.position_y.toInt)) {
                    if ((mouse_position_y.toInt <= city.position_y.toInt + city.size_y.toInt)) {
                        city.is_clicked(is_shift)

                        //For debug purpose only
                        println(f"City ${city.name} is clicked")
                    }
                }
            }
        }
    }
    }
}

def who_is_clicked_buttons(mouse_position_x: Float, mouse_position_y: Float) = {
    //Iterate through all the buttons
    for (button <- side_buttons) {
        if ((mouse_position_x).toInt >= button.px1.toInt) then {
            if ((mouse_position_x) <= button.px2.toInt) then {
                if ((mouse_position_y).toInt >= button.py1.toInt) then {
                    if ((mouse_position_y).toInt <= button.py2.toInt) then {
                        
                        // We found the button clicked, we remove the buttons sprite and we empty the buttons list

                        first_city_clicked.head.buttons_shown = false
                        
                        side_buttons = Nil

                        //Button "create trip" is clicked : game_mode is set to 3
                        if (button.button_name == "create trip") then {
                            game_mode = 3
                            val my_text: Text_print = new Text_print("Creating trip", 20)
                            text_to_print = my_text
                        }

                        //Button "line level" is clicked : game_mode is set to 2
                        if (button.button_name == "line level") then {
                            game_mode = 2
                            val my_text: Text_print = new Text_print("Checking train line info", 20)
                            text_to_print = my_text
                        }

                        //Button "create train" is clicked : a train is created in the station and the player loses 50 million dollars
                        if (button.button_name == "create train") then {
                            if (money >= 50){
                                money -= 50
                                button.city.number_of_train += 1
                                val my_text: Text_print = new Text_print("Train created in city "+button.city.name, 20)
                                text_to_print = my_text
                                game_mode = 0
                                button.city.highlighted = false
                            }
                            else {
                                val not_enough_money: Text_print = new Text_print("Not enough money to create a train", 20)
                                text_to_print = not_enough_money
                            }
                        }

                        //Button "create train line" is clicked : game_mode is set to 1 and loses 100 million dollars
                        if (button.button_name == "create train line") then {
                            if (money >= 100){
                                game_mode = 1
                                val my_text: Text_print = new Text_print("Creating/Upgrading train line", 20)
                                text_to_print = my_text
                            }
                            else {
                                val not_enough_money: Text_print = new Text_print("Not enough money to create a train line", 20)
                                text_to_print = not_enough_money
                            }
                        }
                    }
                }
            }
        }
            
        }
}

//Function to draw a line
def draw_line(x1: Int, y1: Int, x2: Int, y2: Int) ={
    val line = RectangleShape((sqrt( ((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)).toFloat).toInt, 5))
    line.position = (x1 , y1)
    line.rotate((atan2((y2 - y1).toDouble, (x2 - x1).toDouble).toFloat * 180f / 3.14159f).toFloat)
    line
}

//Function to draw a train line
def draw_train_line(train_line : Train_line) = {
    val city1 = train_line.departure
    val city2 = train_line.arrival
    val line = draw_line((city1.position_x + city1.size_x / 2).toInt, (city1.position_y + city1.size_y / 2).toInt, (city2.position_x + city2.size_x / 2).toInt, (city2.position_y + city2.size_y / 2).toInt)
    line.fillColor = train_line.color
    line
}

//Function to get the direction vector from two points in the plane (used to get the direction the train will follow during his trip)
def get_dir_vector(x1: Float, y1: Float, x2: Float, y2: Float) = {

    val dir_vector = (x2 - x1, y2 - y1)
    val dir_vector_norm = sqrt(dir_vector._1 * dir_vector._1 + dir_vector._2 * dir_vector._2)
    (dir_vector._1 / dir_vector_norm, dir_vector._2 / dir_vector_norm)
}

//Function to calculate the distance between two cities
def get_city_distance(city1: City, city2: City) = {
    sqrt((city2.position_x - city1.position_x) * (city2.position_x - city1.position_x) + (city2.position_y - city1.position_y) * (city2.position_y - city1.position_y))
}

//Function to create a train trip : change number of train in departure city, create a train, choose its direction, travel_time, etc...
def create_trip(city1: City, city2: City, speed: Float, is_there_train_line: Boolean, is_merchandise: Boolean) = {
    
    val dir_vector = get_dir_vector(city1.position_x + city1.size_x / 2, city1.position_y + city1.size_y / 2, city2.position_x + city2.size_x / 2, city2.position_y + city2.size_y / 2)
    var s = speed
    def aux_create_trip(train: Train) = {
        train_number += 1
        trains_list = train :: trains_list
        train.travel_time = (get_city_distance(city1, city2) / train.speed).toInt
        city1.number_of_train -= 1
        val passengers = java.lang.Math.min(city1.population / 2, train.capacity)
        money += passengers * 10
        city1.population = city1.population - passengers
        city2.population = city2.population + passengers
        train
    }
    def aux_create_trip2(train: Train) = {
        train_number += 1
        trains_list = train :: trains_list
        train.travel_time = (get_city_distance(city1, city2) / train.speed).toInt
        city1.number_of_train -= 1
        money += train.capacity
        train
    }
    
        if (city1.harbour_links.contains(city2.name)){
            s = 1
            if (!is_merchandise) {
                val boat = new Boat(train_number = train_number, x = city1.position_x + city1.size_x / 2, y = city1.position_y + city1.size_y / 2, dir_x = dir_vector._1.toFloat, dir_y = dir_vector._2.toFloat, departure = city1, arrival = city2, s = s)
                aux_create_trip(boat)
            }
            else {
                val res = city1.resources(0)
                val boat = new Resource_boat(train_number = train_number, x = city1.position_x + city1.size_x / 2, y = city1.position_y + city1.size_y / 2, dir_x = dir_vector._1.toFloat, dir_y = dir_vector._2.toFloat, departure = city1, arrival = city2, s = s, res_on = res)
                aux_create_trip2(boat)
            }

        }
        else if (is_there_train_line){
            if (is_merchandise){
                val res = city1.resources(0)
                val train = new Resource_train(train_number = train_number, x = city1.position_x + city1.size_x / 2, y = city1.position_y + city1.size_y / 2, dir_x = dir_vector._1.toFloat, dir_y = dir_vector._2.toFloat, departure = city1, arrival = city2, s = s, res_on = res)
                aux_create_trip2(train)
            }
            else {
                val train = new Train(train_number = train_number, x = city1.position_x + city1.size_x / 2, y = city1.position_y + city1.size_y / 2, dir_x = dir_vector._1.toFloat, dir_y = dir_vector._2.toFloat, departure = city1, arrival = city2, s = s, txt_name = "train")
                aux_create_trip(train)
            }
        }
        else {
            val plane = new Plane(train_number = train_number, x = city1.position_x + city1.size_x / 2, y = city1.position_y + city1.size_y / 2, dir_x = dir_vector._1.toFloat, dir_y = dir_vector._2.toFloat, departure = city1, arrival = city2, s = 15)
            aux_create_trip(plane)
        }

}

//Updates train : updates positions and check if they are... dead.
def update_all_trains() = {
    for (train <- trains_list) {
        train.update_pos()
        if train.travel_time <= 0 then {
            train.train_arrival.number_of_train += 1
            trains_list = trains_list.filterNot(_.id == train.id)
        }
    }
}