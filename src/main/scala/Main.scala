import scala.util.Using

import sfml.graphics.*
import sfml.window.*

val RESOURCES_DIR = "src/main/resources/"

var sprites_map : Map[String, Sprite] = Map()

@main def main =
    Using.Manager { use =>
        def get_sprite(name: String) = {
            if (sprites_map.contains(name)) then{
                sprites_map(name)
            }
            else{
                val texture = use(Texture())
                    if !(texture.loadFromFile(RESOURCES_DIR + name + ".png")) then System.exit(1)
                val sprite = Sprite(texture)
                sprite
            }
        }

        sprites_map = Map("bar_menu" -> get_sprite("bar_menu"),
                        "boat" -> get_sprite("boat"),
                        "buttons" -> get_sprite("buttons"),
                        "coal_city" -> get_sprite("coal_city"),
                        "fish_city" -> get_sprite("fish_city"),
                        "harbour_city" -> get_sprite("harbour_city"),
                        "map" -> get_sprite("map"),
                        "mountain_city" -> get_sprite("mountain_city"),
                        "plane" -> get_sprite("plane"),
                        "sand_city" -> get_sprite("sand_city"),
                        "start" -> get_sprite("start"),
                        "station" -> get_sprite("station"),
                        "town" -> get_sprite("town"),
                        "train" -> get_sprite("train"))


        /* Create the main window */
        val window = use(RenderWindow(VideoMode(800, 650), "Hello SFML"))
        window.framerateLimit = 30


        /* Create a graphical font to display text*/
        val font = use(Font())
        if !(font.loadFromFile(RESOURCES_DIR + "FiraSans.ttf")) then System.exit(1)
        
        /*Add all cities, and station, a special city defined in Engine.scala, in city_list*/
        city_list = city1 :: city2 :: city3 :: city4 :: city5 :: city6 :: city7 :: city8 :: station :: city_list
        
        init_harbour()
        authorized_roads()
        val view1 = View((400, 325), (800, 650))

        val start_background = get_sprite("start")
        /* Start the game loop */
        while window.isOpen() do
            /*At the very beginning, the player needs to press Alt to start the real game. This section of code is not used once this is done.*/
            while game_mode == -1 do
                
                start_background.position = (0f, 0f)
                window.draw(start_background)
                window.display()
                for event <- window.pollEvent() do
                    event match
                        case Event.Closed()               => window.close()
                        case Event.Resized(width, height) => window.view = View((0, 0, width, height))
                        case Event.KeyReleased(_, alt, control, _, _) => {
                            if (control) {
                                game_mode = 0
                                write_save()
                                map = create_perlin_noise()
                            }
                            if (alt) {
                                game_mode = 0
                                load_save()
                                map = create_perlin_noise()
                            }}
                        case _                            => ()

            /*"Real" game loop starts here*/

            /* Process events */
            for event <- window.pollEvent() do
                event match
                    case Event.Closed()               => {
                        write_save() 
                        window.close()
                    }
                    case Event.Resized(width, height) => window.view = View((0, 0, width, height))
                    case Event.KeyPressed(code, _, _, _, _) => {
                        move_map(view1, window, code)
                    }
                    case Event.KeyReleased(_, alt, control, shift, _) => {

                        /*Alt key acts as click. Thus, who_is_clicked, defined in Engine.scala is called when Alt is released.*/
                        if (alt) {
                            //Get the position of the mouse and convert it to window pixels. Then give it as argument of who_is_clicked.
                            val mouse_position_pixel = Mouse.position(window)
                            val mouse_position = window.mapPixelToCoords(mouse_position_pixel)
                            who_is_clicked(mouse_position.x, mouse_position.y, view1, shift)
                        } 
                        /*Control key is used to comeback to game_mode 0. It can be seen as the way to cancel the ongoing action (Build train, build train_line, etc).*/
                        if (control) {
                            //When entering game_mode 1, the player loses 100 million dollars. He gets a refund if he cancels the action before the end.
                            if (game_mode == 1) {
                                money += 100
                            }
                            if (game_mode != 0) {
                                val my_text: Text_print = new Text_print("Aborted action", 20)
                                text_to_print = my_text
                            }
                            game_mode = 0
                            for (city <- city_list) {
                                city.highlighted = false
                                city.buttons_shown = false
                        }
                        
                    } }
                    case _                            => ()
            if (game_mode != 0){
                println("Game mode is " + game_mode)
            }

            /* update the ressources */

            for (city <- city_list) {
                for (resource_type <- city.biome.biome_production) {
                    var type_found = false
                    for (resource <- city.resources) {
                        if (resource.res_type == resource_type) {
                            type_found = true
                            val rand = new scala.util.Random
                            val r = rand.nextInt(2)
                            if (r == 1) {
                                resource.amount = resource.amount + 1 // une chance sur deux que la resource double
                            }
                        }
                    }
                    if (type_found == false) {
                        val new_resource : Resource = new Resource(resource_type, 1) // on ajoute une ressource au départ pas aléatoirement.
                        city.resources = new_resource :: city.resources
                    }
                }
            }
            // Si l n'y a pas de mission en cours, on en génére une. Dès que cette mission est accomplie, on en recrée une, voir lors de la création de voyage.
            if (!mission_pending) {
                current_mission = generate_mission()
                mission_pending = true
                
            }

            /* Clear the screen */
            window.clear()

            /* Draw the sprite */

            // Draw the grass
            show_map(view1, window)
            
            //Draw all cities
            draw_cities(window)

            //Draw all train lines
            draw_train_lines(window)

            //Draw all trains. (and update them.)
            draw_trains(window)
            update_all_trains()

            
            //Draw the bar menu, the big thing on the right of the window. It is useless now and will be replaced in the near future
            val sprite_bar_menu = get_sprite("bar_menu")
            sprite_bar_menu.position = (view1.center.x + 200, view1.center.y - 325)
            window.draw(sprite_bar_menu)

            //Show money
            //money = money + 1
            draw_money(window, font, view1)
            
            //Display the text to be displayed, on the bottom of the window.
            val text = use(Text(text_to_print.text, font, text_to_print.size))
            val black_bottom_rectangle = RectangleShape((800, 50))
            black_bottom_rectangle.fillColor = Color(0, 0, 0)
            black_bottom_rectangle.position = (view1.center.x - 400, view1.center.y + 275)
            window.draw(black_bottom_rectangle)
            text.position = (view1.center.x - 390, view1.center.y + 285)
            text.fillColor = Color.White()
            window.draw(text)

            //Display the mission text
            val mission_text = use(Text(mission_to_print.text, font, mission_to_print.size))
            mission_text.position = (view1.center.x + 210, view1.center.y - 285)
            mission_text.fillColor = Color.White()
            window.draw(mission_text)
            
            window.view = view1

            /* Update the window */
            window.display()



    }.get



    System.exit(0)